#!/bin/bash
set -e

# Variables to track global status
global_status="SUCCESS"
type="pre-push"
DIR=$(basename "$(pwd)")

# Function to run a command and report its status
run_and_check() {
    local description="$1"
    local cmd="$2"
    
    echo -e "Action: from script ${type}: ${description}: IN PROGRESS"
    if eval "$cmd"; then
        status="SUCCESS"
    else
        status="FAILED"
        global_status="FAILED"
    fi
    echo -e "Action: from script ${type}: ${description}: ${status}"
}

# Define two arrays: one for descriptions and one for commands
descriptions=(
    "Validate the current branch name"
    "Decrypt the local vault file, for encryption"
    "Encrypt the local vault file"
    "Check for secrets in files and update baseline"
    "Audit the secret baseline if needed"
    "Decrypt the local vault file, for Molecule"
    "Lint all README inside the project"
    "Lint all YAML files"
    "Lint the Ansible codebase"
    "Run the whole Molecule test automation"
    "Encrypt the local vault file"
    "Add new editions for the previous tests"
)

commands=(
    "npx --no-install validate-branch-name"
    "MSYS_NO_PATHCONV=1 docker run --rm -v \"$HOME/.docker:/root/.docker\" -v \"\$(pwd):/root/ansible/\${DIR}\" -w /root/ansible/\${DIR} labocbz/ansible-molecule:latest /bin/sh -c 'cat ./.ansible-vault.key > /tmp/.ansible-vault.key && chmod -x /tmp/.ansible-vault.key && (ansible-vault decrypt --vault-password-file /tmp/.ansible-vault.key ./tests/inventory/group_vars/vault.yml || true)'"
    "MSYS_NO_PATHCONV=1 docker run --rm -v \"$HOME/.docker:/root/.docker\" -v \"\$(pwd):/root/ansible/\${DIR}\" -w /root/ansible/\${DIR} labocbz/ansible-molecule:latest /bin/sh -c '(cat ./.ansible-vault.key > /tmp/.ansible-vault.key && chmod -x /tmp/.ansible-vault.key && ansible-vault encrypt --vault-password-file /tmp/.ansible-vault.key ./tests/inventory/group_vars/vault.yml) || true'"
    "MSYS_NO_PATHCONV=1 docker run --rm -v \"\$(pwd):/app\" -w "/app" labocbz/devops-linters:latest detect-secrets scan --exclude-files 'node_modules' --baseline .secrets.baseline"
    "MSYS_NO_PATHCONV=1 docker run --rm -v \"\$(pwd):/app\" -w "/app" labocbz/devops-linters:latest detect-secrets audit .secrets.baseline"
    "MSYS_NO_PATHCONV=1 docker run --rm -v \"$HOME/.docker:/root/.docker\" -v \"\$(pwd):/root/ansible/\${DIR}\" -w /root/ansible/\${DIR} labocbz/ansible-molecule:latest /bin/sh -c 'cat ./.ansible-vault.key > /tmp/.ansible-vault.key && chmod -x /tmp/.ansible-vault.key && (ansible-vault decrypt --vault-password-file /tmp/.ansible-vault.key ./tests/inventory/group_vars/vault.yml || true)'"
    "MSYS_NO_PATHCONV=1 docker run --rm -v \"\$(pwd):/app\" -w "/app" labocbz/devops-linters:latest markdownlint './**.md' --disable MD013"
    "MSYS_NO_PATHCONV=1 docker run --rm -v \"\$(pwd):/app\" -w "/app" labocbz/devops-linters:latest yamllint -c ./.yamllint ."
    "MSYS_NO_PATHCONV=1 docker run --rm -v \"\$(pwd):/app\" -w "/app" labocbz/devops-linters:latest ansible-lint --offline --skip-list=command-instead-of-module --exclude node_modules -x meta-no-info -p ."
    "MSYS_NO_PATHCONV=1 docker run --rm -v \"$HOME/.docker:/root/.docker\" -v \"$HOME/.ssh:/tmp/.ssh:ro\" -v \"\$(pwd):/root/ansible/\${DIR}\" -v \"//var/run/docker.sock:/var/run/docker.sock\" -w /root/ansible/\${DIR} --name molecule-controller--\${DIR} labocbz/ansible-molecule:latest /bin/sh -c 'mkdir -p /root/.ssh && cp /tmp/.ssh/id_rsa* /root/.ssh && chmod 0600 -R /root/.ssh && ssh-keyscan gitlab.com >> ~/.ssh/known_hosts && molecule test'"
    "MSYS_NO_PATHCONV=1 docker run --rm -v \"$HOME/.docker:/root/.docker\" -v \"\$(pwd):/root/ansible/\${DIR}\" -w /root/ansible/\${DIR} labocbz/ansible-molecule:latest /bin/sh -c 'cat ./.ansible-vault.key > /tmp/.ansible-vault.key && chmod -x /tmp/.ansible-vault.key && ansible-vault encrypt --vault-password-file /tmp/.ansible-vault.key ./tests/inventory/group_vars/vault.yml'"
    "git update-index --again"
)

# Run all commands in the specified order
echo "====================== Git hook: ${type} ======================"
for i in "${!descriptions[@]}"; do
    run_and_check "${descriptions[i]}" "${commands[i]}"
    echo "--------------------------------------------------------"
done

# Report the global status
if [ "$global_status" = "SUCCESS" ]; then
    result=0
else
    result=1
fi

echo "====================== Git hook: ${type} ======================"
echo -e "Report: all actions done for hook ${type}, global status: ${global_status}"
echo "====================== Git hook: ${type} ======================"
exit $result
